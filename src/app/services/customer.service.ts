import { Injectable } from '@angular/core';
import {Http, RequestOptions, Headers} from '@angular/http';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  private base_url = 'http://aludra-dev2.azurewebsites.net/MDL03/';
  private auth_key = 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbkFsdWRyYUBoeXBlcm5vdmFsYWJzLmNvbSIsImp0aSI6IjYyYmVmNmU3LTRhNGEtNDM2OS1iNmJmLTdiNmQyNTZlMzQyNSIsImlhdCI6MTUwOTExNjU0MCwicm9sIjoiYXBpX2FjY2VzcyIsImlkIjoiMGY4ZDExMzgtY2JkYy00MzdkLTgyY2QtZWVkZWM1NDg5MDBiIiwibmJmIjoxNTA5MTE2NTM5LCJleHAiOjE1NDA2NTI1MzksImlzcyI6Imh0dHA6Ly9hbHVkcmEtcHJlcHJvZC5henVyZXdlYnNpdGVzLm5ldC8iLCJhdWQiOiJBbHVkcmFBcGlEZXZlbG9wZXJzIn0.kR4rYrnX1lEwQEIb0X19CrmiZoR6jBp4lI7udCnAevE';


  constructor(private http: Http) { }

  getCustomers(){
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.auth_key);

    let options = new RequestOptions({ headers: headers });

    let customer = {
      "searchText": null,
      "customerTypeId": null,
      "roleName": null
    };
    return this.http.post( this.base_url + 'SearchCustomerInfo/Post/TenantId/0', JSON.stringify(customer), options)
  }

  searchCustomers(customer){
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.auth_key);

    let options = new RequestOptions({ headers: headers });
    return this.http.post( this.base_url + 'SearchCustomerInfo/Post/TenantId/0', JSON.stringify(customer), options)
  }
}
