import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerCommercialComponent } from './customer-commercial.component';

describe('CustomerCommercialComponent', () => {
  let component: CustomerCommercialComponent;
  let fixture: ComponentFixture<CustomerCommercialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomerCommercialComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerCommercialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
