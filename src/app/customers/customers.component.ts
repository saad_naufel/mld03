import { Component, OnInit, OnDestroy } from '@angular/core';
import {CustomerService} from '../services/customer.service';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.css']
})
export class CustomersComponent implements OnInit, OnDestroy {
  customers: any[];
  private modalRef: NgbModalRef;
  // closeResult: string;
  constructor(private service: CustomerService, private modalService: NgbModal) {


  }

  ngOnInit() {

    this.service.getCustomers().subscribe(
      response => {
        this.customers = response.json().Data;
        console.log(response.json());
      }
    );
  }

  ngOnDestroy() {
   // this.modalService.;
    //this.activeModal.close();
    this.modalRef.close();
  }

  onCustomerKeyUp(event: any){
    console.log(event.target.value);
    let customer = {
      "searchText": event.target.value,
      "customerTypeId": null,
      "roleName": null
    };

    this.service.searchCustomers(customer).subscribe(
      response => {
        this.customers = response.json().Data;
        console.log(response.json());
      }
    );
  }

  openModal(content) {
    this.modalRef = this.modalService.open(content);
  }

}
