import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BsNavbarComponent } from './bs-navbar/bs-navbar.component';
import { HomeComponent } from './home/home.component';
import { RouterModule} from '@angular/router';
import { NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { CustomersComponent } from './customers/customers.component';
import {CustomerService} from './services/customer.service';
import {HttpModule} from '@angular/http';
import { AddUserComponent } from './add-user/add-user.component';
import { CustomerCommercialComponent } from './customer-commercial/customer-commercial.component';
import { CustomerPersonalComponent } from './customer-personal/customer-personal.component';

@NgModule({
  declarations: [
    AppComponent,
    BsNavbarComponent,
    HomeComponent,
    CustomersComponent,
    AddUserComponent,
    CustomerCommercialComponent,
    CustomerPersonalComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    NgbModule.forRoot(),
    RouterModule.forRoot([
      { path: '', component: HomeComponent},
      { path: 'customers', component: CustomersComponent},
      { path: 'customer/new/commercial', component: CustomerCommercialComponent},
      { path: 'customer/new/personal', component: CustomerPersonalComponent},
    ])
  ],
  providers: [
    CustomerService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
